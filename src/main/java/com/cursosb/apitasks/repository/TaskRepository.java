package com.cursosb.apitasks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursosb.apitasks.model.Task;

public interface TaskRepository extends JpaRepository<Task, Integer> {
}
