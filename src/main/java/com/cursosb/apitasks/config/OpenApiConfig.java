package com.cursosb.apitasks.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class OpenApiConfig {
	/**
	 * Browser: http://localhost:8080/swagger-ui.html
	 * Postman: http://localhost:8080/v3/api-docs
	 * 
	 * @return OpenAPI
	 */
	@Bean
	public OpenAPI customOpenApi() {
		return new OpenAPI()
				.info(new Info()
						.title("REST API for task management")
						.version("1.0.0")
						.description("This API priovide resources for create, list, update and remove task.")
						.termsOfService("")
						.license(new License().name("Apache 2.0").url("https://www.apache.org/licenses/LICENSE-2.0.html")));
	}
}
