package com.cursosb.apitasks.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cursosb.apitasks.model.Task;
import com.cursosb.apitasks.repository.TaskRepository;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("tasks")
@Tag(name = "Task Endpoint", description = "")
public class TaskController {
	@Autowired
	private TaskRepository repository;

	@GetMapping("/status")
	String status() {
		return "I'm OK!";
	}

	@PostMapping("")
	@Operation(summary = "Insert a new task")
	ResponseEntity<Task> insert(@RequestBody Task task) {
		repository.save(task);
		return ResponseEntity.ok(task);
	}

	@GetMapping("")
	@Operation(summary = "Retrieve all tasks")
	ResponseEntity<List<Task>> findAll() {
		return ResponseEntity.ok(repository.findAll());
	}

	@PutMapping("")
	@Operation(summary = "Update task data")
	ResponseEntity<Task> update(@RequestBody Task task) {
		repository.save(task);
		return ResponseEntity.ok(task);
	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Remove a task from database")
	ResponseEntity<String> delete(@PathVariable Integer id) {
		repository.deleteById(id);
		return ResponseEntity.ok("Task deleted!");
	}
}
